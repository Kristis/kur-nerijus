var ctx = document.getElementById('myChart').getContext('2d');
var chart = new Chart(ctx, {
    // The type of chart we want to create
    type: 'line',

    // The data for our dataset
    data: {
        labels: ["PC Panorama", "Senoji Trakų kibininė", "Utenos senamiestis", "Vilnius CodingSchool", "Stotis"],
        datasets: [{
            label: "Nerijaus pasirodymai",
            backgroundColor: 'rgb(6, 29, 66)',
            borderColor: 'rgb(255, 99, 132)',
            data: [2, 1, 3, 15, 6],
        }]
    },

    // Configuration options go here
    options: {}
});

var ctx = document.getElementById('newChart').getContext('2d');
var myDoughnutChart = new Chart(ctx, {
    type: 'doughnut',
    data: {
        labels: ["na na na na na na na na...", "Vėl skalauja jūra krantą", "Vėl nusineša tave"],
        datasets: [{
            label: "S. Povilaitis - Palanga",
            backgroundColor: ['#FB000D', '#ABF000', '#03a9f4'],
            borderColor: '#FFF400',
            data: [40, 30, 30],
        }]
    },

    // Configuration options go here
    options: {}
});

var ctx = document.getElementById('newPie').getContext('2d');
var myPieChart = new Chart(ctx,{
    type: 'pie',
    data: {
        labels: ["Woodoo", "Juodoji magija", "Virbalai"],
        datasets: [{
            label: "My First dataset",
            backgroundColor: ['#009B95', '#33CDC7', '#006561',],
            borderColor: 'rgb(124, 65, 189)',
            data: [40, 70, 30],
        }]
    },

    // Configuration options go here
    options: {}
});

$(document).ready(function(){
      $('.carousel').carousel({
        dist: -200,
      });
      $(".button-collapse").sideNav();

      $(".aurimas").on("click", function(){ open("aurimas.html");
});
      $(".viktorija").on("click", function(){ open("viktorija.html");
});
      $(".jurgita").on("click", function(){ open("jurgita.html");
});

    // $('.carousel.carousel-slider').carousel({
    // });
   });