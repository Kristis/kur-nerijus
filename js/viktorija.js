var ctx = document.getElementById('newPie').getContext('2d');
var myPieChart = new Chart(ctx,{
    type: 'pie',
    data: {
        labels: ["Labai", "Nebelabai"],
        datasets: [{
            label: "My First dataset",
            backgroundColor: ['#33691e', '#c6ff00'],
            borderColor: 'rgb(124, 65, 189)',
            data: [1, 99],
        }]
    },

    // Configuration options go here
    options: {}
});

$(document).ready(function(){
      $('.carousel').carousel({
        dist: -200,
      });
      $(".button-collapse").sideNav();

      $(".aurimas").on("click", function(){ open("aurimas.html");
});
      $(".viktorija").on("click", function(){ open("viktorija.html");
});
      $(".jurgita").on("click", function(){ open("jurgita.html");
});

    // $('.carousel.carousel-slider').carousel({
    // });
   });